import java.util.Scanner;

//The client class with main()
public class Foothill {

	public static void main(String[] args) {
		
		//Create 4 new instances of Property object, with 2 of them loaded with predefined values
		Property prop1 = new Property();
		Property prop2 = new Property();
		Property prop3 = new Property("SFD",390000,2,50,"55 North Parkway, San Diego");
		Property prop4 = new Property("CND",300000,2,23,"224 Main St., San Jose");
		
		//Get user input for property 1
		if(!prop1.SetType(GetType()))
			System.out.println("Error in property type: enter CND, RNT or SFD");
		if(!prop1.SetPrice(GetPrice()))
			System.out.println("Error in property price: cannot be negative");
		if(!prop1.SetBedrooms(GetBedrooms()))
			System.out.println("Error in number of bedrooms: greater than " 
			+ Property.MAX_ROOMS + " number of rooms");
		if(!prop1.SetDaysOnMarket(GetDaysOnMarket()))
			System.out.println("Error in days on market: greater than "
		   + Property.MAX_DAYS_ON_MARKET + " days on market.");
		if(!prop1.SetAddress(GetAddress()))
			System.out.println("Error in property address: cannot be null");
		System.out.println();
		
		//Get user input for property 2
		if(!prop2.SetType(GetType()))
			System.out.println("Error in property type: enter CND, RNT or SFD");
		if(!prop2.SetPrice(GetPrice()))
			System.out.println("Error in property price: cannot be negative");
		if(!prop2.SetBedrooms(GetBedrooms()))
			System.out.println("Error in number of bedrooms: greater than" 
			+ Property.MAX_ROOMS + "number of rooms");
		if(!prop2.SetDaysOnMarket(GetDaysOnMarket()))
			System.out.println("Error in days on market: greater than"
		   + Property.MAX_DAYS_ON_MARKET + " days on market.");
		if(!prop2.SetAddress(GetAddress()))
			System.out.println("Error in property address: cannot be null");
		
		//Convert property 3 which is SFD to RNT using ConvertToRental()
		prop3.ConvertToRental();
		
		//Print property listings
		System.out.println("\nProperty Listings:");
		prop1.PrintListing();
		prop2.PrintListing();
		prop3.PrintListing();
		prop4.PrintListing();
		
	}
	
	//Gets type input from user
	static String GetType() {
		
		Scanner input = new Scanner(System.in);
		String string_in;
		
		System.out.print("What is the type of property? (CND,RNT,SFD): ");
		string_in = input.next();
		
		return string_in;
	
	}
	
	//Gets price input from user
	static int GetPrice() {
		
		Scanner input = new Scanner(System.in);
		String string_in;
		
		System.out.print("What is the property price?: ");
		string_in = input.next();
		
		return Integer.parseInt(string_in);
		
	}
	
	//Gets number of bedrooms input from user
	static int GetBedrooms() {
		
		Scanner input = new Scanner(System.in);
		String string_in;
		
		System.out.print("How many bedrooms?: ");
		string_in = input.next();
		
		return Integer.parseInt(string_in);
		
	}
	
	//Gets days on market input from user
	static int GetDaysOnMarket() {
		
		Scanner input = new Scanner(System.in);
		String string_in;
		
		System.out.print("How many days on the market?: ");
		string_in = input.next();
		
		return Integer.parseInt(string_in);
		
	}
	
	//Gets address input from user
	static String GetAddress() {
		
		Scanner input = new Scanner(System.in);
		String string_in;
		
		System.out.print("What is address of the property?: ");
		string_in = input.nextLine();
		
		return string_in;
	
	}

}
