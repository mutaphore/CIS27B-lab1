//The property class
public class Property {
	
	//Declare class members
	public static final int MAX_DAYS_ON_MARKET = 1825;
	public static final int MAX_ROOMS = 50;
	public static final int RENTAL_CONVERSION_FACTOR = 300;
	public static final String RENTAL = "RNT";
	public static final String CONDO = "CND";
	public static final String SINGLE_FAMILY = "SFD";
	public static final String DEFAULT_TYPE = CONDO;
	public static final int DEFAULT_PRICE = 2000;
	public static final int DEFAULT_BEDROOMS = 3;
	public static final int DEFAULT_DAYS = 30;
	public static final String DEFAULT_ADDRESS = "123 Street, San Diego";
	private String type;
	private int price;
	private int bedrooms;
	private int days_on_market;
	private String address;
	
	//Default Constructor
	public Property() {
		
		this.type = DEFAULT_TYPE;
		this.price = DEFAULT_PRICE;
		this.bedrooms = DEFAULT_BEDROOMS;
		this.days_on_market = DEFAULT_DAYS;
		this.address = DEFAULT_ADDRESS;
		
	}
	
	//Constructor with 5 params passed, use mutators to check validity of input
	public Property(String type, int price, int bedrooms, int days_on_market, String address) {
		
		if(!SetType(type)) {
			System.out.println("Error in property type: enter CND, RNT or SFD");
			this.type = DEFAULT_TYPE;
		}
		if(!SetPrice(price)) {
			System.out.println("Error in property price: cannot be negative");
			this.price = DEFAULT_PRICE;
		}
		if(!SetBedrooms(bedrooms)) {
			System.out.println("Error in number of bedrooms: greater than "
			+ Property.MAX_ROOMS + " number of rooms");
			this.bedrooms = DEFAULT_BEDROOMS;
		}
		if(!SetDaysOnMarket(days_on_market)) {
			System.out.println("Error in days on market: greater than "
			+ Property.MAX_DAYS_ON_MARKET + " days on market.");
			this.days_on_market = DEFAULT_DAYS;
		}
		if(!SetAddress(address)) {
			System.out.println("Error in property address: cannot be null");
			this.address = DEFAULT_ADDRESS;
		}
		
	}
	
	//Mutators to set private members
	boolean SetType(String type) {
		
		if((!type.equals(RENTAL)) && (!type.equals(CONDO)) && (!type.equals(SINGLE_FAMILY))) 
			return false;
	
		this.type = type;
		return true;
		
	}
	
	boolean SetPrice(int price) {
		
		if(price < 0)
			return false;
		this.price = price;
		return true;
		
	}
	
	boolean SetBedrooms(int bedrooms) {
		
		if(bedrooms < 0 || bedrooms > MAX_ROOMS	)
			return false;
		this.bedrooms = bedrooms;
		return true;
		
	}

	boolean SetDaysOnMarket(int days_on_market) {
		
		if(days_on_market < 0 || days_on_market > MAX_DAYS_ON_MARKET)
			return false;
		this.days_on_market = days_on_market;
		return true;
		
	}
	
	boolean SetAddress(String address) {
		
		if(address.equals(""))
			return false;
		this.address = address;
		return true;
		
	}
	
	//Accessors to get private members
	
	String GetType() { return type; }
	int GetPrice() {return price; }
	int GetBedrooms() {return bedrooms; }
	int GetDaysOnMarket() {return days_on_market; }
	String GetAddress() {return address; }
	
	//This method converts current instance to rental type and price
	void ConvertToRental() {
		
		this.type = RENTAL;
		this.price /= RENTAL_CONVERSION_FACTOR;
		
	}
	
	//Prints the current instance property information in a sentence.
	void PrintListing() {
		
		System.out.print("This property is a ");
		
		if(type.equals(RENTAL))
			System.out.print("Rental ");
		else if(type.equals(CONDO))
			System.out.print("Condo ");
		else
			System.out.print("Single Family Dwelling ");
		
		System.out.print("with " + bedrooms + " bedrooms and has " 
		+ "been on the market for " + days_on_market + " days.");
		
		System.out.print(" Located at " + address 
		+ ", it is currently priced at $" + price +"\n");
		
	}

}
